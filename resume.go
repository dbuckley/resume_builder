package resume_builder

import "time"

type Resume struct {
	Font string

	Name    string
	Tagline string

	ContactDetails []Contact

	Objective []string

	Skills     Skills
	Experience []Experience
	Education  []Education
	Projects   []string
	Accolades  []string
}

type Contact struct {
	Text string
	Link string
}

type Skills map[string]Skill

type Skill []string

type Experience struct {
	Company  string
	Location string

	Roles []Role
}

type Role struct {
	Title string

	StartDate time.Time
	EndDate   time.Time

	Accomplishments []string
}

type Education struct {
	University string

	Degree string

	StartDate time.Time
	EndDate   time.Time

	GPA float64

	Accomplishments []string
}

type Getter interface {
	Resume() (Resume, error)
}

func (s Skill) AsList() string {
	l := len(s)
	if l >= 3 {
		r := ""
		for i, j := range s {
			if i == 0 {
				r = j
			} else if i == l-1 {
				r += ", and " + j
			} else {
				r += ", " + j
			}
		}
		return r
	} else if l == 2 {
		return s[0] + " and " + s[1]
	} else if l == 1 {
		return s[0]
	}
	return ""
}
