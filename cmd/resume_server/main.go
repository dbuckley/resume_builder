package main

import (
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/gobuffalo/packr"
	"github.com/pkg/errors"
	"github.com/spf13/pflag"
	"github.com/thecodingmachine/gotenberg-go-client/v5"
	"gitlab.com/dbuckley/resume_builder"
	"gitlab.com/dbuckley/resume_builder/markdown"
)

func main() {
	// Create flags for our optional variables
	port := pflag.IntP("port", "p", 3000, "the port to start the CYOA web application on")
	source := pflag.StringP("source", "s", "test.md", "source of the resume")
	template := pflag.StringP("template", "t", "default", "template to use for displaying the resume")
	pflag.Parse()

	tmpl, err := loadTemplate(*template)
	if err != nil {
		io.WriteString(os.Stdout, "Failed to load template: "+err.Error()+"\n")
	}

	c := gotenberg.Client{Hostname: "http://localhost:3030"}

	g := markdown.NewFile(*source)
	h := resume_builder.NewHandler(g, resume_builder.WithTemplate(tmpl))
	http.Handle("/", h)
	http.HandleFunc("/resume.pdf", getPDF(&c, h, *port))

	fs := http.FileServer(http.Dir("static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	fmt.Printf("Starting the server on port: %d\n", *port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", *port), nil))
}

func loadTemplate(option string) (*template.Template, error) {
	b := packr.NewBox("./views")

	tf, err := b.FindString(option + ".gohtml")
	if err != nil {
		return nil, errors.New("could not find template " + option)
	}

	t, err := template.New("").Parse(tf)
	if err != nil {
		return nil, errors.Wrap(err, "failed to glob templates from views directory")
	}

	return t, nil
}

func getPDF(c *gotenberg.Client, h *resume_builder.Handler, port int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		f, err := ioutil.TempFile("/tmp", "resume-")
		if err != nil {
			log.Print(err)
		}

		body, err := h.String()
		if err != nil {
			log.Print(err)
		}

		if _, err := f.Write([]byte(body)); err != nil {
			log.Print(err)
		}

		req, err := gotenberg.NewHTMLRequest(f.Name())
		if err != nil {
			log.Print(errors.Wrap(err, "could not create new html request"))
			return
		}

		req.PaperSize(gotenberg.Letter)
		req.Margins([4]float64{0.0, 0.0, 0.0, 0.0})
		resp, err := c.Post(req)
		if err != nil {
			log.Print(err)
		}

		io.Copy(w, resp.Body)
		os.Remove(f.Name())
		f.Close()
	}
}
