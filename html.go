package resume_builder

import (
	"bytes"
	"html/template"
	"log"
	"net/http"

	"github.com/pkg/errors"
)

func init() {
	tpl = template.Must(template.New("").Parse(defaultTemplate))
}

var tpl *template.Template
var defaultTemplate = `
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>{{.Name}}</title>
  </head>
  <body>
    <section class="page">
      <h1>{{.Name}}</h1>
      {{if .ContactDetails}}
	  <section class="contact">
		<p>{{range .ContactDetails}}
			<a href={{.Link}}>{{.Text}}</a><br/>
		{{end}}</p>
	  </section>
      {{end}}
      {{if .Summary}}
	  <p>{{.Summary}}</p>
	  {{end}}
	  {{if .Experience}}{{range .Experience}}<section class="experience">
	  <p><div>
		<div class="cdiv"><strong>{{.Company}}</div>
		<div class="cdiv"><strong>{{.From }}</div>
	  </p>
	  {{end}}{{end}}
    </section>
    <style>
      body {
        font-family: {{if .Font}}{{.Font}},{{end}} arial;
      }
      h1 {
        text-align:center;
        position:relative;
      }
      .page {
        width: 80%;
        max-width: 500px;
        margin: auto;
        margin-top: 40px;
        margin-bottom: 40px;
        padding: 80px;
        background: #FFFCF6;
        border: 1px solid #eee;
        box-shadow: 0 10px 6px -6px #777;
      }
	  .contact {
	    text-align:center;
	  }
      ul {
        border-top: 1px dotted #ccc;
        padding: 10px 0 0 0;
        -webkit-padding-start: 0;
      }
      li {
        padding-top: 10px;
      }
      a,
      a:visited {
        text-decoration: none;
        color: #6295b5;
      }
      a:active,
      a:hover {
        color: #7792a2;
      }
      p {
        text-indent: 1em;
      }
    </style>
  </body>
</html>`

type Handler struct {
	r   Resume
	t   *template.Template
	get Getter
}

func NewHandler(get Getter, opts ...HandlerOptions) *Handler {
	h := &Handler{
		get: get,
		t:   tpl,
	}

	for _, fn := range opts {
		fn(h)
	}

	return h
}

type HandlerOptions func(h *Handler)

func WithTemplate(t *template.Template) HandlerOptions {
	return func(h *Handler) {
		h.t = t
	}
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "Not a valid method", http.StatusBadRequest)
		return
	}

	res, err := h.get.Resume()
	if err != nil {
		log.Print(errors.Wrap(err, "failed to get the resume"))
		http.Error(w, "Something went wront...", http.StatusInternalServerError)
		return
	}
	if err := h.t.Execute(w, res); err != nil {
		log.Print(errors.Wrap(err, "failed to template resume"))
		http.Error(w, "Something went wrong...", http.StatusInternalServerError)
	}
}

func (h *Handler) String() (string, error) {
	b := &bytes.Buffer{}
	res, err := h.get.Resume()
	if err != nil {
		return "", err
	}
	if err := h.t.Execute(b, res); err != nil {
		return "", err
	}

	return b.String(), nil
}
