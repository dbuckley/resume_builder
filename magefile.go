//+build mage

package main

import (
	"os"
	"os/exec"

	fswatch "github.com/andreaskoch/go-fswatch"
	"github.com/magefile/mage/sh"
	"github.com/sirupsen/logrus"
)

func Development() error {
	logrus.SetLevel(logrus.DebugLevel)

	logrus.Debug("Strating development server")

	var (
		first = make(chan struct{}, 1)
		cmd   *exec.Cmd
	)
	first <- struct{}{}

	w := fswatch.NewFolderWatcher(".", true, func(path string) bool {
		return false
	}, 1)

	w.Start()
	for w.IsRunning() {
		select {
		case <-first:
		case <-w.ChangeDetails():
		}

		if cmd != nil {
			logrus.Debug("restarting server")
			if cmd.Process != nil {
				if err := cmd.Process.Kill(); err != nil {
					return err
				}
			}
		}

		logrus.Debug("building the binary")
		err := sh.RunV("packr", "build", "-o", "/tmp/resume_server", "./cmd/resume_server")
		if err != nil {
			logrus.Error("failed to start server: ", err.Error())
			continue
		}

		logrus.Debug("running server")
		cmd = exec.Command("/tmp/resume_server")
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		if err := cmd.Start(); err != nil {
			logrus.Error("failed to start server: ", err.Error())
		}
	}

	return nil
}
