module gitlab.com/dbuckley/resume_builder

go 1.12

require (
	github.com/andreaskoch/go-fswatch v1.0.0
	github.com/gobuffalo/packr v1.30.1
	github.com/gomarkdown/markdown v0.0.0-20190222000725-ee6a7931a1e4
	github.com/google/go-cmp v0.3.0
	github.com/magefile/mage v1.8.0
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/pflag v1.0.3
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/thecodingmachine/gotenberg-go-client/v5 v5.0.1
	gitlab.com/golang-commonmark/html v0.0.0-20180917080848-cfaf75183c4a // indirect
	gitlab.com/golang-commonmark/linkify v0.0.0-20180917065525-c22b7bdb1179 // indirect
	gitlab.com/golang-commonmark/markdown v0.0.0-20181102083822-772775880e1f
	gitlab.com/golang-commonmark/mdurl v0.0.0-20180912090424-e5bce34c34f2 // indirect
	gitlab.com/golang-commonmark/puny v0.0.0-20180912090636-2cd490539afe // indirect
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7 // indirect
	golang.org/x/text v0.3.2 // indirect
)
