package markdown

import (
	"regexp"
	"strconv"
	"strings"

	"github.com/gomarkdown/markdown/ast"
	"gitlab.com/dbuckley/resume_builder"
)

type education struct {
	entries []resume_builder.Education
}

var _ parser = &experience{}

func (e *education) isNode(n ast.Node) bool {
	_, ok := n.(*ast.Paragraph)
	return ok
}

func (e *education) parseNode(n ast.Node, level int) {
	i := len(e.entries) - 1
	if level == 2 {
		i++
		e.entries = append(e.entries, resume_builder.Education{})
	}

	if _, ok := n.(*ast.Paragraph); !ok {
		return
	}

	uncleText := ""
	if level == 6 {
		gramp := n.GetParent().GetParent().GetParent()
		uncle := ast.GetFirstChild(gramp)
		if _, ok := uncle.(*ast.Paragraph); ok {
			uncleText = childTextSearch(uncle)
		}
	}

	isAccomp := regexp.MustCompile(`(?i)^\s?accomp(lishment)?s?`)

	text := childTextSearch(n)
	if level == 2 && e.entries[i].University == "" {
		e.entries[i].University = text
	} else if level == 4 {
		isDegree := regexp.MustCompile(`(?i)^(master|bachelor)'?s?`)
		isGPA := regexp.MustCompile(`(?i)^gpa\s?[:-]?\s`)

		switch {
		case isDegree.MatchString(text):
			e.entries[i].Degree = text

		case isGPA.MatchString(text):
			sn := isGPA.ReplaceAllString(text, "")
			gpa, err := strconv.ParseFloat(sn, 64)
			if err == nil {
				e.entries[i].GPA = gpa
			}

		case strings.HasPrefix(text, "From"):
			rg := regexp.MustCompile(`[fF][rR][oO][mM]\s?\W?\s`)
			date := getDate(rg.ReplaceAllString(text, ""))
			e.entries[i].StartDate = date

		case strings.HasPrefix(text, "To"):
			rg := regexp.MustCompile(`[tT][oO]\s?\W?\s`)
			date := getDate(rg.ReplaceAllString(text, ""))
			e.entries[i].EndDate = date
		}

	} else if isAccomp.MatchString(uncleText) {
		e.entries[i].Accomplishments = append(e.entries[i].Accomplishments, text)
	}
}
