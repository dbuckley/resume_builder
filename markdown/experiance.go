package markdown

import (
	"regexp"
	"strings"

	"github.com/gomarkdown/markdown/ast"
	"gitlab.com/dbuckley/resume_builder"
)

type experience struct {
	e []resume_builder.Experience
}

var _ parser = &experience{}

func (e *experience) isNode(n ast.Node) bool {
	_, ok := n.(*ast.Paragraph)
	return ok
}

func (e *experience) parseNode(n ast.Node, level int) {
	i := len(e.e) - 1
	if level == 2 {
		i++
		e.e = append(e.e, resume_builder.Experience{})
	}
	text := childTextSearch(n)

	if level == 2 && e.e[i].Company == "" {
		beforeDash := regexp.MustCompile(`^.* - `)
		afterDash := regexp.MustCompile(` - .*$`)

		title := afterDash.ReplaceAllString(text, "")
		loc := beforeDash.ReplaceAllString(text, "")
		if loc == title {
			loc = ""
		}

		e.e[i].Company = title
		e.e[i].Location = loc
		return
	}

	if _, ok := n.(*ast.Paragraph); !ok {
		return
	}

	var parentList string
	if level > 4 {
		pl := n.GetParent().GetParent().GetParent()
		plc := ast.GetFirstChild(pl)
		plcc := ast.GetFirstChild(plc)
		parentList = string(childTextSearch(plcc))
	}

	r := len(e.e[i].Roles) - 1
	if level == 4 {
		e.e[i].Roles = append(e.e[i].Roles, resume_builder.Role{})
		r = len(e.e[i].Roles) - 1
		e.e[i].Roles[r].Title = text
		return
	}

	if level == 6 {
		switch {
		case strings.HasPrefix(text, "From"):
			rg := regexp.MustCompile(`[fF][rR][oO][mM]\s?\W?\s`)
			date := getDate(rg.ReplaceAllString(text, ""))
			e.e[i].Roles[r].StartDate = date

		case strings.HasPrefix(text, "To"):
			rg := regexp.MustCompile(`[tT][oO]\s?\W?\s`)
			date := getDate(rg.ReplaceAllString(text, ""))
			e.e[i].Roles[r].EndDate = date
		}
	} else if level == 8 && parentList == "Accomplishments" {
		e.e[i].Roles[r].Accomplishments = append(e.e[i].Roles[r].Accomplishments, text)
	}

	// else if level == 4 {
	// isTitle := regexp.MustCompile(`(?i)^(as|as|title)\s?[a:]?\s`)

	// } else if isAccomp.MatchString(uncleText) {
	// }
}
