package markdown

import (
	"bytes"
	"io"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/ast"
	"github.com/pkg/errors"
	"gitlab.com/dbuckley/resume_builder"
)

var months = map[string]time.Month{
	"jan": time.January,
	"feb": time.February,
	"mar": time.March,
	"apr": time.April,
	"may": time.May,
	"jun": time.June,
	"jul": time.July,
	"aug": time.August,
	"sep": time.September,
	"oct": time.October,
	"nov": time.November,
	"dec": time.December,
}

type File struct {
	src string
}

func NewFile(src string) *File {
	return &File{
		src: src,
	}
}

func (mf *File) Resume() (resume_builder.Resume, error) {
	if strings.HasPrefix(mf.src, "http") {
		return httpGet(mf.src)
	} else {
		return localGet(mf.src)
	}
}

func httpGet(string) (resume_builder.Resume, error) {
	return resume_builder.Resume{}, nil
}

func localGet(file string) (resume_builder.Resume, error) {
	f, err := os.Open(file)
	if err != nil {
		return resume_builder.Resume{}, err
	}
	defer f.Close()

	r, err := ParseResume(f)
	if err != nil {
		return resume_builder.Resume{}, errors.Wrap(err, "failed to parse resume from the file")
	}

	return r, nil
}

func ParseResume(in io.Reader) (resume_builder.Resume, error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(in)
	node := markdown.Parse(buf.Bytes(), nil)

	r := resume_builder.Resume{}
	nodes := node.GetChildren()
	for _, n := range nodes {
		hd, ok := n.(*ast.Heading)
		if !ok {
			continue
		}

		headerText := childTextSearch(hd)
		if hd.Level == 1 && r.Name == "" {
			r.Name = headerText
		}

		switch headerText {
		case "Objective":
			s := paragraphs(ast.GetNextNode(hd))
			if len(s) == 0 {
				return resume_builder.Resume{}, errors.New("objective defined but no content found")
			}
			r.Objective = s

		case "Tagline":
			ss := paragraphs(ast.GetNextNode(hd))
			if len(ss) == 0 {
				return resume_builder.Resume{}, errors.New("tagline defined but no content found")
			}
			r.Tagline = strings.Join(ss, "\n")

		case "Contact":
			c := contact{}
			parseList(&c, ast.GetNextNode(hd), 0)
			r.ContactDetails = c.entries

		case "Experience":
			exp := experience{}
			parseList(&exp, ast.GetNextNode(hd), 0)
			r.Experience = exp.e

		case "Education":
			ed := education{}
			parseList(&ed, ast.GetNextNode(hd), 0)
			r.Education = ed.entries

		case "Skills":
			s := skills{}
			parseList(&s, ast.GetNextNode(hd), 0)
			r.Skills = s.entries

		case "Projects":
			l := flatList{}
			parseList(&l, ast.GetNextNode(hd), 0)
			r.Projects = l

		case "Accolades":
			l := flatList{}
			parseList(&l, ast.GetNextNode(hd), 0)
			r.Accolades = l
		}
	}
	return r, nil
}

type parser interface {
	parseNode(ast.Node, int)
	isNode(ast.Node) bool
}

type flatList []string

var _ parser = &flatList{}

func (l *flatList) isNode(n ast.Node) bool {
	_, ok := n.(*ast.Paragraph)
	return ok
}

func (l *flatList) parseNode(n ast.Node, level int) {
	*l = append(*l, childTextSearch(n))
}

func getDate(d string) time.Time {
	isYear := regexp.MustCompile(`\b[0-9]{4}\b`)
	isYearDate := regexp.MustCompile(`^[0-9]{4} [a-zA-Z]*$`)
	isDateYear := regexp.MustCompile(`^[a-zA-Z]* [0-9]{4}$`)

	switch {
	case isYearDate.MatchString(d):
		s := strings.Split(d, " ")
		yr, err := strconv.Atoi(s[0])
		if err != nil {
			return time.Time{}
		}
		m := strings.ToLower(s[1])[:3]
		return time.Date(yr, months[m], 1, 0, 0, 0, 0, time.UTC)
	case isDateYear.MatchString(d):
		s := strings.Split(d, " ")
		yr, err := strconv.Atoi(s[1])
		if err != nil {
			return time.Time{}
		}
		m := strings.ToLower(s[0])[:3]
		return time.Date(yr, months[m], 1, 0, 0, 0, 0, time.UTC)
	case isYear.MatchString(d):
		yr, err := strconv.Atoi(d)
		if err != nil {
			return time.Time{}
		}
		return time.Date(yr, time.January, 1, 0, 0, 0, 0, time.UTC)
	}
	return time.Time{}
}

func isParagraph(n ast.Node) bool {
	_, ok := n.(*ast.Paragraph)
	return ok
}

func parseList(p parser, n ast.Node, level int) {
	in := ""
	for i := 0; i < level; i++ {
		in = in + " "
	}
	if p.isNode(n) {
		_, ok := n.GetParent().(*ast.ListItem)
		if !ok {
			return
		}

		p.parseNode(n, level)
	}

	for c := ast.GetFirstChild(n); c != nil; c = ast.GetNextNode(c) {
		parseList(p, c, level+1)
	}
}

func paragraphs(n ast.Node) []string {
	str := []string{}
	for c := n; isParagraph(c); c = ast.GetNextNode(c) {
		str = append(str, childTextSearch(c))
	}
	return str
}

func childTextSearch(n ast.Node) string {
	str := strings.Builder{}
	if txt, ok := n.(*ast.Text); ok {
		return string(txt.AsLeaf().Literal)
	}

	for c := ast.GetFirstChild(n); c != nil; c = ast.GetNextNode(c) {
		str.WriteString(childTextSearch(c))
	}
	return str.String()
}
