package markdown_test

import (
	"fmt"
	"io"
	"strings"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/dbuckley/resume_builder"
	"gitlab.com/dbuckley/resume_builder/markdown"
)

func dateByYear(year int) time.Time {
	return time.Date(year, time.January, 1, 0, 0, 0, 0, time.UTC)
}

func dateByMonYear(month time.Month, year int) time.Time {
	return time.Date(year, month, 1, 0, 0, 0, 0, time.UTC)
}

func markdownWithBody(s string) io.Reader {
	return strings.NewReader(fmt.Sprintf(`# Mr Nobody

%s`, s))
}

var defName string = "Mr Nobody"

func TestGetResume(t *testing.T) {
	tTable := []struct {
		name   string
		input  string
		expect resume_builder.Resume
	}{
		{
			name:  "get name and header",
			input: "## Objective\n\nthis is my objective.",
			expect: resume_builder.Resume{
				Name:      defName,
				Objective: []string{"this is my objective."},
			},
		}, {
			name: "get experience",
			input: `## Experience
- Fast Food - Revere, MA
	- Patty Flipper
		- From 2013
		- To 2015
- Pencil Pushers Inc. - Boston, MA
	- Pencil Pusher
		- From 2015
		- To 2018
		- Accomplishments
			- one`,
			expect: resume_builder.Resume{
				Name: "Mr Nobody",
				Experience: []resume_builder.Experience{{
					Company:  "Fast Food",
					Location: "Revere, MA",
					Roles: []resume_builder.Role{{
						Title:     "Patty Flipper",
						StartDate: dateByYear(2013),
						EndDate:   dateByYear(2015),
					}},
				}, {
					Company:  "Pencil Pushers Inc.",
					Location: "Boston, MA",
					Roles: []resume_builder.Role{{
						Title:           "Pencil Pusher",
						StartDate:       dateByYear(2015),
						EndDate:         dateByYear(2018),
						Accomplishments: []string{"one"},
					}},
				}},
			},
		}, {
			name:  "get tagline",
			input: "## Tagline\n\nhire me now please.",
			expect: resume_builder.Resume{
				Name:    defName,
				Tagline: "hire me now please.",
			},
		}, {
			name:  "get contacts",
			input: "## Contact\n\n- 123456789\n- [joe](joe@testing.test)",
			expect: resume_builder.Resume{
				Name: defName,
				ContactDetails: []resume_builder.Contact{
					{Text: "123456789", Link: ""},
					{Text: "joe", Link: "joe@testing.test"},
				},
			},
		}, {
			name: "get education",
			input: `## Education

- Pencil Pushers University
	- master's in pencils
    - From 2009
    - To 2013
	- GPA 4.5
    - Accomplishments
        - one`,
			expect: resume_builder.Resume{
				Name: defName,
				Education: []resume_builder.Education{{
					University:      "Pencil Pushers University",
					Degree:          "master's in pencils",
					StartDate:       dateByYear(2009),
					EndDate:         dateByYear(2013),
					GPA:             4.5,
					Accomplishments: []string{"one"},
				}},
			},
		}, {
			name: "get date input",
			input: `## Experience

- Pencil Pusher - Pen, Ma
	- Pencil Pusher
		- From January 2018
		- To Jan 2019`,
			expect: resume_builder.Resume{
				Name: defName,
				Experience: []resume_builder.Experience{{
					Company:  "Pencil Pusher",
					Location: "Pen, Ma",
					Roles: []resume_builder.Role{{
						Title:     "Pencil Pusher",
						StartDate: dateByMonYear(time.January, 2018),
						EndDate:   dateByMonYear(time.January, 2019),
					}},
				}},
			},
		}, {
			name: "get skills",
			input: `## Skills

- Programming
	- Perl
	- Lisp`,
			expect: resume_builder.Resume{
				Name: "Mr Nobody",
				Skills: resume_builder.Skills{
					"Programming": []string{"Perl", "Lisp"},
				},
			},
		}, {
			name: "get projects",
			input: `## Projects

- Built a pencil
- Lifted a whole sleeve of paper`,
			expect: resume_builder.Resume{
				Name: defName,
				Projects: []string{
					"Built a pencil",
					"Lifted a whole sleeve of paper",
				},
			},
		}, {
			name: "full resume",
			input: `## Tagline

The best in the business

Contact
-------

- [Derek](derek@email.com)

Objective
-------

I'm super awesome and you should hire me!

Experience
----

- Fast Food
	- Patty Flipper
		- From 2013
		- To 2015
- Pencil Pushers Inc.
	- Pencil Pusher
		- From 2015
		- To 2018
		- Accomplishments
			- one

Education
---------

- Pencil Pushers University
    - From 2009
    - To 2013
	- GPA 4.5
    - Accomplishments
        - one

Accolades
---------

- Member of pencil pushing union`,
			expect: resume_builder.Resume{
				Name:           defName,
				Tagline:        "The best in the business",
				ContactDetails: []resume_builder.Contact{{Text: "Derek", Link: "derek@email.com"}},
				Objective:      []string{"I'm super awesome and you should hire me!"},
				Experience: []resume_builder.Experience{{
					Company: "Fast Food",
					Roles: []resume_builder.Role{{
						Title:     "Patty Flipper",
						StartDate: dateByYear(2013),
						EndDate:   dateByYear(2015),
					}},
				}, {
					Company: "Pencil Pushers Inc.",
					Roles: []resume_builder.Role{{
						Title:           "Pencil Pusher",
						StartDate:       dateByYear(2015),
						EndDate:         dateByYear(2018),
						Accomplishments: []string{"one"},
					}},
				}},
				Education: []resume_builder.Education{{
					University:      "Pencil Pushers University",
					StartDate:       dateByYear(2009),
					EndDate:         dateByYear(2013),
					GPA:             4.5,
					Accomplishments: []string{"one"},
				}},
				Accolades: []string{"Member of pencil pushing union"},
			},
		},
	}

	for _, ts := range tTable {
		t.Run(ts.name, func(t *testing.T) {
			in := markdownWithBody(ts.input)
			got, err := markdown.ParseResume(in)
			if err != nil {
				t.Fatal(err)
			} else if !cmp.Equal(ts.expect, got) {
				t.Error(cmp.Diff(ts.expect, got))
			}
		})
	}
}
