package markdown

import (
	"github.com/gomarkdown/markdown/ast"
	"gitlab.com/dbuckley/resume_builder"
)

type skills struct {
	entries resume_builder.Skills
}

var _ parser = &contact{}

func (s *skills) isNode(n ast.Node) bool {
	_, ok := n.(*ast.Paragraph)
	return ok
}

func (s *skills) parseNode(n ast.Node, level int) {
	if s.entries == nil {
		s.entries = resume_builder.Skills{}
	}

	text := childTextSearch(n)
	if level == 2 {
		if _, ok := s.entries[text]; !ok {
			s.entries[text] = []string{}
		}
	} else if level == 4 {
		var parentList string
		pl := n.GetParent().GetParent().GetParent()
		plc := ast.GetFirstChild(pl)
		plcc := ast.GetFirstChild(plc)
		parentList = string(childTextSearch(plcc))

		s.entries[parentList] = append(s.entries[parentList], text)
	}
}
