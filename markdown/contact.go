package markdown

import (
	"github.com/gomarkdown/markdown/ast"
	"gitlab.com/dbuckley/resume_builder"
)

type contact struct {
	entries []resume_builder.Contact
}

var _ parser = &contact{}

func (c *contact) isNode(n ast.Node) bool {
	_, okP := n.(*ast.Paragraph)
	_, okL := n.(*ast.Link)
	return (okP || okL)
}

func (c *contact) parseNode(n ast.Node, level int) {
	ch := ast.GetFirstChild(n)
	if _, ok := ch.(*ast.Text); !ok {
		return
	}

	text := childTextSearch(n)
	dest := ""
	sib := ast.GetNextNode(ch)
	if l, ok := sib.(*ast.Link); ok {
		dest = string(l.Destination)
	}

	c.entries = append(c.entries, resume_builder.Contact{
		Text: text,
		Link: dest,
	})
}
